= Expected results

The files `ui.yml`, `content1.js`, and `subdir/content2.js` should be in

* `work/static`
* `public/_`
* the root of the output `build/static-files-ui-bundle.zip`
